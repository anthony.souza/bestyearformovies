# Best Year for Movies

## Pré-requisitos

o que você vai precisar:

- `Python` ao menos na **versao 3.6**
- Jupyter Notebook


## Iniciando o servidor de desenvolvimento local

### Agora iremos instalar todas as dependências necessárias e iniciar o container para a execução da aplicação. Execute o comando abaixo em seu terminal

```bash
pip3 install -r requirements.txt
```

## Aplicando as "Migrations" em seu Banco de dados Local

### Para aplicar as **migrations**. Execute o comando abaixo em seu terminal

```bash
docker-compose run --rm web python manage.py migrate --noinput
```

## Documentação

Documentação Complete Sobre esse projeto disponível em : [`docs/`](docs).
